# Google+ Go Quick-Start

The documentation for this quick-start is maintained on developers.google.com.
Please see here for more information:
https://developers.google.com/+/quickstart/go 

This fork of https://github.com/googleplus/gplus-quickstart-go implements the same functionality but on App Engine.
